# README #
This is C code for arduino,
the number of analog input are limited in Arduino and in some cases the extension of these inputs are essential

The "74HC4050" IC is needed for this extention

  	            __  __
	input#4  --|  \/  |-- 5v in
	input#6  --|      |-- input#2
	OUT      --|      |-- input#1
	input#7  --|      |-- input#0
	input#5  --|      |-- input#3
	Ground   --|      |-- Digital 1
	Ground   --|      |-- Digital 2
	Ground   --|______|-- Digital 3

Table

	binary table of digital pins
	I        D1 D2 D3
	input#0  0  0  0
	input#1  0  0  1
	input#2  0  1  0
	input#3  0  1  1
	input#4  1  0  0
	input#5  1  0  1
	input#6  1  1  0
	input#7  1  1  1
Notes

	be careful on the noise of the input source. it can give you the very wrong results if you dont fix it.
	it is recommended to measure the drop and shift of the setup for longterm run.
	the noise for arduino is high (specially for acc record), find the most suitable time for your setup first.


depends on your need and device the pause (delay) should be adjusted.
The required time to make the capacitor empty and load again is needed while time interval is short.
keep in your mind the serial communication baud (usually 9600 is the default, for higher number in this code 2M is recommended)